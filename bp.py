import data
import network
import sys

# training_data, test_data = data.load(train_size=50000,test_size=5000)
# net = network.Network([784, 40, 10])
# net.SGD(training_data, epochs=30, mini_batch_size=10, learning_rate=0.1, test_data=test_data)

class QuestionFlow:

    def __init__(self):
        self.training_data = None
        self.test_data = None

    def run(self, subsection):
        invokes = {'2a': self.Qa, '2b': self.Qb, '2c': self.Qc, '2d': self.Qd}
        print('{0} returned value: {1}'.format(subsection, invokes[subsection]()))

    def Qa(self):
        self.training_data, self.test_data = data.load(train_size=10000, test_size=5000)
        net = network.Network([784, 40, 10])
        return net.SGD(self.training_data, epochs=30, mini_batch_size=10, learning_rate=0.1, test_data=self.test_data)

    def Qb(self):
        self.training_data, self.test_data = data.load(train_size=10000, test_size=5000)
        net = network.Network([784, 40, 10])
        return net.SGD(self.training_data, epochs=30, mini_batch_size=10, learning_rate=0.1, test_data=self.test_data, Qb=True)

    def Qc(self):
        self.training_data, self.test_data = data.load(train_size=50000, test_size=5000)
        net = network.Network([784, 40, 10])
        return net.SGD(self.training_data, epochs=30, mini_batch_size=10, learning_rate=0.1, test_data=self.test_data)

    def Qd(self):
        self.training_data, self.test_data = data.load(train_size=10000, test_size=5000)
        net = network.Network([784, 30, 30, 30, 30, 10])
        return net.SGD(self.training_data, epochs=30, mini_batch_size=10000, learning_rate=0.1, test_data=self.test_data, Qd=True)

if __name__ == "__main__":
    qf = QuestionFlow()
    qf.run(sys.argv[1])