"""
network.py
"""

import random
import numpy as np
import math
from sgd import plot as plotfnc

class Network(object):

    def __init__(self, sizes):
        """The list ``sizes`` contains the number of neurons in the
        respective layers of the network.  For example, if the list
        was [2, 3, 1] then it would be a three-layer network, with the
        first layer containing 2 neurons, the second layer 3 neurons,
        and the third layer 1 neuron.  The biases and weights for the
        network are initialized randomly, using a Gaussian
        distribution with mean 0, and variance 1.  Note that the first
        layer is assumed to be an input layer, and by convention we
        won't set any biases for those neurons, since biases are only
        ever used in computing the outputs from later layers."""
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
        self.weights = [np.random.randn(y, x)
                        for x, y in zip(sizes[:-1], sizes[1:])]

    def SGD(self, training_data, epochs, mini_batch_size, learning_rate,
            test_data, Qb=False, Qd=False):
        """Train the neural network using mini-batch stochastic
        gradient descent.  The ``training_data`` is a list of tuples
        ``(x, y)`` representing the training inputs and the desired
        outputs.  """
        print("Initial test accuracy: {0}".format(self.one_label_accuracy(test_data)))
        n = len(training_data)
        Epochs = [i for i in range(epochs)]
        TrainingAccuracy, TrainingLoss, TestAccuracy = [], [], []
        dbs = []
        for j in range(epochs):
            random.shuffle(list(training_data))
            mini_batches = [
                training_data[k:k+mini_batch_size]
                for k in range(0, n, mini_batch_size)]
            for mini_batch in mini_batches:
                dbs.append([d/len(training_data) for d in self.update_mini_batch(mini_batch, learning_rate, getDB=True)])

            test_acc = self.one_label_accuracy(test_data)
            print ("Epoch {0} test accuracy: {1}".format(j, test_acc))
            if Qb:
                TrainingAccuracy.append(self.one_hot_accuracy(training_data))
                TrainingLoss.append(self.loss(training_data))
                TestAccuracy.append(test_acc)

        if Qb:
            plotfnc('2b_1.png', Epochs, [TrainingAccuracy], title='2B (i) - Training accuracy across epochs',
                    xlabel='Epoch', ylabel='Accuracy')
            plotfnc('2b_2.png', Epochs, [TrainingLoss], title='2B (ii) - Training loss across epochs',
                    xlabel='Epoch', ylabel='Loss')
            plotfnc('2b_3.png', Epochs, [TestAccuracy], title='2B (iii) - Test accuracy across epochs',
                    xlabel='Epoch', ylabel='Accuracy')

        if Qd:
            Ys = np.transpose(dbs)
            plotfnc('2d.png', Epochs, Ys, title='2D - dl/db(i) across epochs',
                    xlabel='Epoch', ylabel='Norm of dl/db(i)', legends=['i={0}'.format(i+1) for i in range(len(Ys))])

        return test_acc


    def update_mini_batch(self, mini_batch, learning_rate, getDB=False):
        """Update the network's weights and biases by applying
        stochastic gradient descent using backpropagation to a single mini batch.
        The ``mini_batch`` is a list of tuples ``(x, y)``."""
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        db = [0.0] * len(self.biases)
        for x, y in mini_batch:
            delta_nabla_b, delta_nabla_w = self.backprop(x, y)
            for i in range(len(db)):
                db[i] += np.linalg.norm(delta_nabla_b[i])
            nabla_b = [nb+dnb for nb, dnb in zip(nabla_b, delta_nabla_b)]
            nabla_w = [nw+dnw for nw, dnw in zip(nabla_w, delta_nabla_w)]
        self.weights = [w - (learning_rate / len(mini_batch)) * nw
                        for w, nw in zip(self.weights, nabla_w)]
        self.biases = [b - (learning_rate / len(mini_batch)) * nb
                       for b, nb in zip(self.biases, nabla_b)]

        if getDB:
            return db

    def backprop(self, x, y):
        output, Z, A = self.network_output_before_softmax(x, True)
        db = [self.loss_derivative_wr_output_activations(output, y)]
        for w, z in reversed(zip(self.weights[1:], Z[:-1])):
            db.append(np.multiply(np.dot(np.transpose(w), db[-1]), sigmoid_derivative(z)))

        db, dw = db[::-1], []
        for a, d in zip(A, db):
            dw.append(np.dot(d, np.transpose(a)))
        return db, dw

    def one_label_accuracy(self, data):
        """Return accuracy of network on data with numeric labels"""
        output_results = [(np.argmax(self.network_output_before_softmax(x)), y)
         for (x, y) in data]
        return sum(int(x == y) for (x, y) in output_results)/float(len(data))

    def one_hot_accuracy(self,data):
        """Return accuracy of network on data with one-hot labels"""
        output_results = [(np.argmax(self.network_output_before_softmax(x)), np.argmax(y))
                          for (x, y) in data]
        return sum(int(x == y) for (x, y) in output_results) / float(len(data))


    def network_output_before_softmax(self, x, keepZ=False):
        """Return the output of the network before softmax if ``x`` is input."""
        layer = 0
        z, a = [], [x]
        for b, w in zip(self.biases, self.weights):
            if layer == len(self.weights) - 1:
                x = np.dot(w, x) + b
                z.append(x)
            else:
                z.append(np.dot(w, x) + b)
                x = sigmoid(np.dot(w, x)+b)
                a.append(x)
            layer += 1

        if keepZ:
            return x, z, a
        return x

    def loss(self, data):
        """Return the loss of the network on the data"""
        loss_list = []
        for (x, y) in data:
            net_output_before_softmax = self.network_output_before_softmax(x)
            net_output_after_softmax = self.output_softmax(net_output_before_softmax)
            loss_list.append(np.dot(-np.log(net_output_after_softmax).transpose(),y).flatten()[0])
        return sum(loss_list) / float(len(data))

    def output_softmax(self, output_activations):
        """Return output after softmax given output before softmax"""
        output_exp = np.exp(output_activations)
        return output_exp/output_exp.sum()

    def loss_derivative_wr_output_activations(self, output_activations, y):
        """Return derivative of loss with respect to the output activations before softmax"""
        return self.output_softmax(output_activations) - y


def sigmoid(z):
    """The sigmoid function."""
    return 1.0/(1.0+np.exp(-z))

def sigmoid_derivative(z):
    """Derivative of the sigmoid function."""
    return sigmoid(z)*(1-sigmoid(z))

def multiplot(filename, xarr, yarrs, title=None, xlabel=None, ylabels=None, grid=True):
    fig = plt.figure()
    plt.plot(xarr, yarr)
    plt.xscale(xscale)
    if (title != None):
        plt.title(title)
    if (xlabel != None):
        plt.xlabel(xlabel)
    if (ylabel != None):
        plt.ylabel(ylabel)
    if (grid):
        plt.grid()
    if (points_of_interest != None):
        for p in points_of_interest:
            plt.plot(p[0], p[1], '*')
            plt.annotate("({0},{1})".format(p[0], p[1]), xy=(p[0], p[1]))
    fig.savefig(filename)
    plt.close()
