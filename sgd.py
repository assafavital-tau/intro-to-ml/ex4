from numpy import *
import numpy.random
from sklearn.datasets import fetch_mldata
import sklearn.preprocessing
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt
import sys

def plot(filename, xarr, yarr,
         xscale='linear', yscale='linear', title=None, xlabel=None, ylabel=None, legends=None, grid=True, points_of_interest=None):
    '''
    Create a plot from matplotlib
    :param filename: Name of output .PNG file
    :param xarr: Array corresponding to x values
    :param yarr: Array corresponding to y values
    :param xscale: Scale of x axis (default = 'linear')
    :param yscale: Scale of y axis (default = 'linear')
    :param title: Title of plot (optional)
    :param xlabel: Label of x axis (optional)
    :param ylabel: Label of y axis (optional)
    :param grid: Visibility of grid (shown by default)
    :param points_of_interest: Mark points of interest above the said plot (optional)
    :return: 
    '''

    if legends is None:
        legends = [None for y in yarr]
    fig = plt.figure()
    for i in range(len(yarr)):
        plt.plot(xarr, yarr[i], label=legends[i])
    plt.xscale(xscale)
    if(title != None):
        plt.title(title)
    if(xlabel != None):
        plt.xlabel(xlabel)
    if(ylabel != None):
        plt.ylabel(ylabel)
    if(grid):
        plt.grid()
    if(points_of_interest != None):
        for p in points_of_interest:
            plt.plot(p[0], p[1], '*')
            plt.annotate("({0},{1})".format(p[0], p[1]), xy=(p[0], p[1]))
    plt.legend()
    fig.savefig(filename)
    plt.close()

class SGD:

    def __init__(self):
        '''
        Initialize the dataset
        '''

        mnist = fetch_mldata('MNIST original')
        data = mnist['data']
        labels = mnist['target']

        neg, pos = 0, 8
        train_idx = numpy.random.RandomState(0).permutation(where((labels[:60000] == neg) | (labels[:60000] == pos))[0])
        test_idx = numpy.random.RandomState(0).permutation(where((labels[60000:] == neg) | (labels[60000:] == pos))[0])

        train_data_unscaled = data[train_idx[:6000], :].astype(float)
        self._train_labels = (labels[train_idx[:6000]] == pos) * 2 - 1

        validation_data_unscaled = data[train_idx[6000:], :].astype(float)
        self._validation_labels = (labels[train_idx[6000:]] == pos) * 2 - 1

        test_data_unscaled = data[60000 + test_idx, :].astype(float)
        self._test_labels = (labels[60000 + test_idx] == pos) * 2 - 1

        # Preprocessing
        self._train_data = sklearn.preprocessing.scale(train_data_unscaled, axis=0, with_std=False)
        self._validation_data = sklearn.preprocessing.scale(validation_data_unscaled, axis=0, with_std=False)
        self._test_data = sklearn.preprocessing.scale(test_data_unscaled, axis=0, with_std=False)

    def _calculate_average_accuracy(self, w, test=False):
        '''
        Calculate the average accuracy of SGD training.
        :param w: Weights vector
        :param test: If true, calculate accuracy according to test set. Else, according to validation set.
        :return: Accuracy of SGF (0-1)
        '''

        accuracy = 0.0
        set, labels = self._validation_data, self._validation_labels
        if(test):
            set, labels = self._test_data, self._test_labels
        m = len(set)
        for i in range(m):
            sample, label = set[i], labels[i]
            dp = numpy.dot(w, sample) * label
            accuracy += (dp > 1)

        return accuracy / m

    def train(self, C, H0, T):
        '''
        Train the SGD
        :param C: C parameter
        :param H0: eta_0 parameter
        :param T: Number of iterations
        :return: W-vector of weights
        '''

        m = len(self._train_data)
        w = numpy.zeros(len(self._train_data[0]))
        for t in range(1, T + 1):
            i = numpy.random.randint(m)
            sample, label = self._train_data[i], self._train_labels[i]
            dp = numpy.dot(w, sample) * label
            Ht = H0 / t
            w = numpy.float32(1-Ht) * w + ((dp < 1) * (Ht * C * label * sample))

        return w


class QuestionFlow:

    def __init__(self):
        self._sgd = SGD()
        self._bestH0 = None
        self._bestC = None
        self._bestW = None

    def run(self, subsection):
        invokes = {'1a' : self.Qa, '1b' : self.Qb, '1c' : self.Qc, '1d' : self.Qd}
        print('{0} returned value: {1}'.format(subsection, invokes[subsection]()))

    def Qa(self):
        C = 1
        T = 1000
        H = [10 ** x for x in range(-4, 3)]
        accuracies = []
        iterations = 10
        for H0 in H:
            accuracy = 0.0
            for i in range(iterations):
                w = self._sgd.train(C, H0, T)
                accuracy += self._sgd._calculate_average_accuracy(w)
            accuracies += [accuracy / iterations]

        bestH0 = numpy.argmax(accuracies)
        plot('1a.png', H, [accuracies], xscale='log', title='1A - SGD accuracy as a function of eta0',
             xlabel='eta0', ylabel='Accuracy rate', points_of_interest=[(H[bestH0], accuracies[bestH0])])

        self._bestH0 = H[bestH0]
        return H[bestH0]

    def Qb(self):
        T = 1000
        bestH0 = self._bestH0
        if (bestH0 == None):
            bestH0 = self.Qa()
        C = [10 ** (0.5*x) for x in range(-10, 20)]
        accuracies = []
        iterations = 10
        for c in C:
            accuracy = 0.0
            for i in range(iterations):
                w = self._sgd.train(c, bestH0, T)
                accuracy += self._sgd._calculate_average_accuracy(w)
            accuracies += [accuracy / iterations]

        bestC = numpy.argmax(accuracies)
        plot('1b.png', C, [accuracies], xscale='log', title='1B - SGD accuracy with best eta0 as a function of C',
             xlabel='C', ylabel='Accuracy rate', points_of_interest=[(C[bestC], accuracies[bestC])])

        self._bestC = C[bestC]
        return C[bestC]

    def Qc(self):
        T = 20000
        bestH0, bestC = self._bestH0, self._bestC
        if(bestH0 == None):
            bestH0 = self.Qa()
        if(bestC == None):
            bestC = self.Qb()

        w = self._sgd.train(bestC, bestH0, T)
        fig = plt.figure()
        plt.imshow(reshape(w, (28, 28)), interpolation='nearest')
        plt.title('1C - Representation of w given best eta0 and best C')
        fig.savefig('1c.png')
        plt.close()

        self._bestW = w
        return w

    def Qd(self):
        w = self._bestW
        if(w == None):
            w = self.Qc()

        accuracy = self._sgd._calculate_average_accuracy(w, True)
        return accuracy

if __name__ == "__main__":
    qf = QuestionFlow()
    qf.run(sys.argv[1])